FROM node:8

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

EXPOSE 3000
CMD [ "node", "./bin/www" ]

# To build the image do "docker image build -t <some name of app> ."
# To run the container do "docker container run -d --name <random name to id container> -p 3025:3000 <name of app from step above>"
# You can now go to http://localhost:3025 and see your app running.