const express = require('express');

const router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  res.render('index', {
    title: 'Express - Home'
  });
});

router.get('/generic', (req, res) => {
  res.render('generic', {
    title: 'Generic Page'
  });
});

router.get('/elements', (req, res) => {
  res.render('elements', {
    title: 'Elements Page'
  });
});

module.exports = router;
