module.exports = {
  "extends": "airbnb-base",
  "env": {
    "browser": true,
    "node": true,
    "es6": true,
    "mocha": true,
    "mongo": true,
    "jquery": true
  },
  "rules": {
    "no-console": "off",
    "comma-dangle": ["error", "never"],
    "no-var": "off",
    "vars-on-top": "off",
    "object-shorthand": ["error", "never"],
    "prefer-destructuring": [
      "error", {
        "VariableDeclarator": {
          "array": false,
          "object": false
        },
        "AssignmentExpression": {
          "array": false,
          "object": false
        }
      }, {
        "enforceForRenamedProperties": false
      }
    ]
  }
};