const formValidationSchema = {
  name: {
    isLength: {
      options: {
        min: 2,
        max: 100
      },
      errorMessage: 'Must be between 2 and 100 characters'
    },
    isAlphanumeric: {
      errorMessage: 'Must be alphanumeric and not contain special characters'
    }
  },
  email: {
    isEmail: {
      options: {
        allow_display_name: false,
        require_display_name: false,
        allow_utf8_local_part: true,
        require_tld: true,
        allow_ip_domain: false,
        domain_specific_validation: false
      },
      errorMessage: 'Must be a valid email address'
    }
  },
  message: {
    isLength: {
      options: {
        min: 2,
        max: 500
      },
      errorMessage: 'Must be between 2 and 500 characters'
    },
    isAlphanumeric: {
      errorMessage: 'Must be alphanumeric and not contain special characters'
    }
  }
};

module.exports = formValidationSchema;
