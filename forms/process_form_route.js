const express = require('express');
const { checkSchema } = require('express-validator/check');
const { validationResult } = require('express-validator/check');
const formValidationSchema = require('./form_validation_schema');
const processForm = require('./process_form');

const router = express.Router();

const errorFormatter = ({ location, msg, param }) => `${location}[${param}]: ${msg}`;

/* GET users listing. */
// eslint-disable-next-line consistent-return
router.post('/', checkSchema(formValidationSchema), (req, res) => {
  const result = validationResult(req).formatWith(errorFormatter);
  if (!result.isEmpty()) {
    return res.json({
      status: 'failed',
      data: {
        errors: result.array()
      }
    });
  }
  if (result.isEmpty()) {
    processForm(req.body, (response) => {
      res.json({
        status: 'success',
        data: response
      });
    });
  }
});

module.exports = router;
